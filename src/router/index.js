import Vue from "vue";
import VueRouter from "vue-router";

// Views
import Sites from "../views/Sites.vue";
import Site from "../views/Site.vue";
import Clients from "../views/Clients.vue";
import Client from "../views/Client.vue";
import Me from "../views/Me.vue";
import ErrorPage from "../views/Error.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Sites",
    component: Sites,
  },
  {
    path: "/sites/:id",
    name: "Site",
    component: Site,
  },
  {
    path: "/clients/",
    name: "Clients",
    component: Clients,
  },
  {
    path: "/clients/:id",
    name: "Client",
    component: Client,
  },
  {
    path: "/me",
    name: "Me",
    component: Me,
  },
  {
    path: "*",
    name: "Error",
    component: ErrorPage,
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
