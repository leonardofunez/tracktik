import axios from "axios";

const headers = {
  "Content-Type": "application/json",
};

const baseURL = "https://tracktik-challenge.staffr.com/";

const apiTrackTik = axios.create({
  baseURL,
  withCredentials: false,
  headers,
});

export default {
  getSites(page, sort = "createdAt", order = "desc") {
    return apiTrackTik(
      `sites?_page=${page}&_limit=9&_sort=${sort}&_order=${order}`
    );
  },

  getSite(id) {
    return apiTrackTik(`sites/${id}`);
  },

  getClients(page, sort = "createdAt", order = "desc") {
    return apiTrackTik(
      `clients?_page=${page}&_limit=9&_sort=${sort}&_order=${order}`
    );
  },

  getClient(id) {
    return apiTrackTik(`clients/${id}`);
  },

  getClientSites(id, page, sort = "createdAt", order = "desc") {
    return apiTrackTik(
      `clients/${id}/sites?_page=${page}&_limit=9&_sort=${sort}&_order=${order}`
    );
  },

  getMe() {
    return apiTrackTik("me");
  },
};
