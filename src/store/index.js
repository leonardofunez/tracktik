import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    is_loading: true,
  },

  mutations: {
    set_is_loading: (state, data) => (state.is_loading = data),
  },

  actions: {
    SET_IS_LOADING: (context, data) => context.commit("set_is_loading", data),
  },

  getters: {
    GET_IS_LOADING: (state) => state.is_loading,
  },
});
